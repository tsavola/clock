/* $Id: clock.c,v 1.13 2004-05-15 21:19:34 timo Exp $
 *
 * Copyright (c) 2003, 2004 timo.savola@iki.fi
 * Licensed under the GNU General Public License version 2.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>

#include <glib.h>
#include <pango/pango.h>
#include <gtk/gtk.h>

#define STYLE_FMT \
"	style \"clock\" {" \
"		font_name = \"%s\"" \
"		fg[NORMAL] = \"%s\"" \
"		bg[NORMAL] = \"%s\"" \
"	}" \
"	widget \"*\" style \"clock\""

#define USAGE_FMT \
	"%s [-f|--font <description>]" \
	  " [-o|--offset <value>]" \
	  " [-F|--foreground <color>]" \
	  " [-B|--background <color>]\n"

static char *get_time(void)
{
	static char buf[1024];
	time_t t1;
	struct tm *t2;

	t1 = time(NULL);
	t2 = localtime(&t1);

	strftime(buf, sizeof (buf), "  %a %e %b  %k:%M  ", t2);

	return buf;
}

static gboolean update_label(GtkWidget *label)
{
	gtk_label_set_text(GTK_LABEL(label), get_time());

	return TRUE;
}

static void usage(FILE *out,
		  const char *progname)
{
	fprintf(out, USAGE_FMT, progname);
}

static void read_args(int argc,
		      char **argv,
		      char **fontp,
		      int *offsetp,
		      char **fgp,
		      char **bgp)
{
	struct option longopts[] = {
		{ "help",       no_argument,       0,      'h' },
		{ "version",    no_argument,       0,      'v' },
		{ "font",       required_argument, 0,      'f' },
		{ "offset",     required_argument, 0,      'o' },
		{ "foreground", required_argument, 0,      'F' },
		{ "background", required_argument, 0,      'B' },
		{ NULL }
	};

	while (1) {
		int c;

		c = getopt_long(argc, argv, "hvf:o:F:B:", longopts, NULL);
		if (c < 0) {
			break;
		}

		switch (c) {
		case 'f':
			if (strlen(optarg) > 128) {
				fprintf(stderr, "%s: font name is too long\n",
					argv[0]);
				exit(1);
			}
			*fontp = optarg;
			break;

		case 'o':
			*offsetp = atoi(optarg);
			break;

		case 'F':
			if (strlen(optarg) > 16) {
				fprintf(stderr, "%s: bad foreground color\n",
					argv[0]);
				exit(1);
			}
			*fgp = optarg;
			break;

		case 'B':
			if (strlen(optarg) > 16) {
				fprintf(stderr, "%s: bad background color\n",
					argv[0]);
				exit(1);
			}
			*bgp = optarg;
			break;

		case 'v':
			printf("clock " VERSION "\n");
			exit(0);

		case 'h':
			fprintf(stdout, USAGE_FMT, argv[0]);
			exit(0);

		default:
			fprintf(stderr, USAGE_FMT, argv[0]);
			exit(1);
		}
	}

	if (optind < argc) {
		fprintf(stderr, USAGE_FMT, argv[0]);
		exit(1);
	}
}

int main(int argc,
	 char **argv)
{
	int fd;
	GtkWidget *window, *label;
	char *font = "monospace";
	int scr_width, scr_height;
	int win_width = 0, win_height = 0;
	int offset = 0;
	int x, y;
	char *fg = "white", *bg = "black";
	static char style[1024];

	gtk_init(&argc, &argv);
	read_args(argc, argv, &font, &offset, &fg, &bg);

	sprintf(style, STYLE_FMT, font, fg, bg);

	gtk_rc_parse_string(style);

	window = gtk_window_new(GTK_WINDOW_POPUP);
	gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
	gtk_window_stick(GTK_WINDOW(window));

	label = gtk_label_new(get_time());
	gtk_container_add(GTK_CONTAINER(window), label);

	gtk_widget_show_all(window);

	scr_width = gdk_screen_get_width(gdk_screen_get_default());
	scr_height = gdk_screen_get_height(gdk_screen_get_default());
	gtk_window_get_size(GTK_WINDOW(window), &win_width, &win_height);

	x = (scr_width - win_width) / 2;
	y = scr_height - win_height - offset;
	gtk_window_move(GTK_WINDOW(window), x, y);

	g_timeout_add(15000, (GSourceFunc) update_label, label);

	gtk_main();
	return 0;
}
