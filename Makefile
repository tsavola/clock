ifndef PREFIX
PREFIX = /usr/local
endif

ifndef BINDIR
BINDIR = $(PREFIX)/bin
endif

ifndef PKG_CONFIG
PKG_CONFIG = pkg-config
endif

clock: clock.c
	$(CC) -DVERSION=\"`head -n1 clock.c|cut -d' ' -f4`\" \
		$(CFLAGS) -o $@ $^ $(LDFLAGS) \
		`$(PKG_CONFIG) --cflags --libs gtk+-2.0`

install: clock
	install -d $(BINDIR)
	install -m 755 -s clock $(BINDIR)

clean:
	rm -f clock
